package com.province.proviceapi.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @Column(name = "name", nullable = false)
	private String name;

    @Column(name = "prefix", nullable = false)
	private String prefix;
    
    @OneToMany(mappedBy = "district", cascade=CascadeType.ALL)
    @JsonManagedReference
	private Set<Ward> ward;

    @ManyToOne
    @JoinColumn(name = "province_id")
    @JsonIgnore
    private  Province province;
    
    public District() {
    }

    public District(int id, String name, String prefix, Set<Ward> ward, Province province) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.ward = ward;
        this.province = province;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Set<Ward> getWard() {
        return ward;
    }

    public void setWard(Set<Ward> ward) {
        this.ward = ward;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    
}
