package com.province.proviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProviceApiApplication.class, args);
	}

}
