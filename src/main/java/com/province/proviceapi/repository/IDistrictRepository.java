package com.province.proviceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.province.proviceapi.models.District;

public interface IDistrictRepository extends JpaRepository <District, Long>{
    District findById(int id);
    
}
