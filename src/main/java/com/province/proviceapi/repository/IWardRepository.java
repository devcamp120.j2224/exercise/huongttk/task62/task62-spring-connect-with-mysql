package com.province.proviceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.province.proviceapi.models.Ward;

public interface IWardRepository extends JpaRepository <Ward, Long>{
    
}
