package com.province.proviceapi.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.province.proviceapi.models.Province;

public interface IProvinceRepository extends JpaRepository <Province, Long>{
   Optional<Province>findByCode(String code);
}
