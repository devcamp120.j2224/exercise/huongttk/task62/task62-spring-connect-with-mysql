package com.province.proviceapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.province.proviceapi.models.District;
import com.province.proviceapi.models.Province;
import com.province.proviceapi.models.Ward;
import com.province.proviceapi.repository.IDistrictRepository;
import com.province.proviceapi.repository.IProvinceRepository;



@RestController
@CrossOrigin
@RequestMapping
public class CProvinceController {
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;
    
    @GetMapping("/devcamp-provinces")
    public ResponseEntity<List<Province>> getProvinceList() {
        try {
            List<Province> provinceList = new ArrayList<Province>();
            provinceRepository.findAll().forEach(provinceList::add);
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-districts")
    public ResponseEntity<Set<District>> getDistrictListByProvinceCode(@RequestParam(value = "code") String code) {
        try {
            Optional<Province> vProvince = provinceRepository.findByCode(code);
            if(vProvince != null) {
                return new ResponseEntity<>(vProvince.get().getDistrict(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-wards")
    public ResponseEntity<Set<Ward>> getWardListByDistrictId(@RequestParam(value = "districtId") int districtId) {
        try {
            District vDistrict = districtRepository.findById(districtId);
            if(vDistrict != null) {
                return new ResponseEntity<>(vDistrict.getWard(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
